import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import DropDownMenu from '@/components/DropDownMenu.vue';
import vuetify from "@/plugins/vuetify";
import type { ItemMenu } from '@/types/ItemMenu';

const listItems: Array<ItemMenu> = [
  {title: 'Add 1 + 1', method: () => {}},
  {title: '1 - 1', method: () => {}}
]

describe('DropDownMenu.vue', () => {

  it('import and mount component without props', () => {
    expect(DropDownMenu).toBeTruthy();

    const wrapper = mount(DropDownMenu, {
      global: {
        plugins: [vuetify]
      },
    });

    expect(wrapper.html()).toBeTruthy();
  });

  it('import and mount component with props', () => {
    expect(DropDownMenu).toBeTruthy();

    const wrapper = mount(DropDownMenu, {
      global: {
        plugins: [vuetify]
      },
      props: {
        items: listItems,
        title: 'Test button',
        tooltip: 'Test'
      }
    });

    expect(wrapper.html()).toBeTruthy();
  });
});
# Drop-down-menu

A component for setting up a dropdown menu.

## Props

| Props | Type | default | Description | Optional |
| ----- | ---- | ------- | ----------- | -------- |
| items | Array<ItemMenu> | [] | List of items | No |
| tooltip | string | '' | Tooltip display | Yes |
| title | string | '' | Button text | Yes |


## Types 

### ItemMenu

| Attribut | Type | Description |
| -------- | ---- | ----------- |
| title | string | Text display in the menu |
| method | Function | Method launch at click |

## Use the package in another project

```sh
npm i -D @metabohub/drop-down-menu
```

If your project is not using vuetify, you need to import it in `src/main.ts` :
```ts
import { createApp } from 'vue'
import App from './App.vue'
import { vuetify } from "@metabohub/drop-down-menu";

createApp(App).use(vuetify).mount('#app')
```

Use the component : 
```ts
<script setup lang="ts">
import type { ItemMenu } from "@metabohub/drop-down-menu/dist/types/ItemMenu";
import { DropDownMenu } from "@metabohub/drop-down-menu";

const fileItems: Array<ItemMenu> = [
	{title: 'Load file', method: openLoadFile},
	{title: 'Save as Json', method: saveNetwork},
];

</script>

<template>
  <DropDownMenu
    class="mt-2 ml-2 mb-2"
    title="File"
    tooltip="File management"
    :items="fileItems"
  />
</template>

<style>
import "@metabohub/drop-down-menu/dist/style.css";
</style>
```